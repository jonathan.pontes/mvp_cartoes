package com.cartoes.mvp.cartao.service;

import com.cartoes.mvp.cartao.model.Cartao;
import com.cartoes.mvp.cartao.repository.CartaoRepository;
import com.cartoes.mvp.client.model.Client;
import com.cartoes.mvp.client.service.ClientService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClientService clientService;

    public Cartao create(Cartao cartao) {
        // Bloco de validação
        Client client = clientService.getById(cartao.getClient().getId());
        cartao.setClient(client);

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new RuntimeException("Cartão já existente com esse número!");
        }

        // Regras de negócio
        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Long id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ObjectNotFoundException(CartaoService.class, "Cartao não encontrado!");
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new ObjectNotFoundException(CartaoService.class, "Cartao não encontrado!");
        }

        return byId.get();
    }

}
