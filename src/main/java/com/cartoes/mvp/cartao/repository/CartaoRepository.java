package com.cartoes.mvp.cartao.repository;


import com.cartoes.mvp.cartao.model.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Long> {

    Optional<Cartao> findByNumero(String numero);

}
