package com.cartoes.mvp.client.repository;


import com.cartoes.mvp.client.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
