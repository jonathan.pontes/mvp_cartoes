package com.cartoes.mvp.client.service;


import com.cartoes.mvp.cartao.service.CartaoService;
import com.cartoes.mvp.client.model.Client;
import com.cartoes.mvp.client.repository.ClientRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client create(Client client) {
        return clientRepository.save(client);
    }

    public Client getById(Long id) {
        Optional<Client> byId = clientRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ObjectNotFoundException(CartaoService.class, "Cartao não encontrado!");
        }

        return byId.get();
    }

}
