package com.cartoes.mvp.pagamento.controller;

import com.cartoes.mvp.pagamento.model.Pagamento;
import com.cartoes.mvp.pagamento.model.dto.CreatePagamentoRequest;
import com.cartoes.mvp.pagamento.model.dto.PagamentoResponse;
import com.cartoes.mvp.pagamento.model.mapper.PagamentoMapper;
import com.cartoes.mvp.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = PagamentoMapper.toPagamento(createPagamentoRequest);

        pagamento = pagamentoService.create(pagamento);

        return PagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<PagamentoResponse> listByCartao(@PathVariable Long cartaoId) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoId);
        return PagamentoMapper.toPagamentoResponse(pagamentos);
    }

}
