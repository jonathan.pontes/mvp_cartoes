package com.cartoes.mvp.pagamento.service;

import com.cartoes.mvp.cartao.model.Cartao;
import com.cartoes.mvp.cartao.service.CartaoService;
import com.cartoes.mvp.pagamento.model.Pagamento;
import com.cartoes.mvp.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Pagamento create(Pagamento pagamento) {
        Cartao cartao = cartaoService.getById(pagamento.getCartao().getId());

        pagamento.setCartao(cartao);

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listByCartao(Long cartaoId) {
        return pagamentoRepository.findAllByCartao_id(cartaoId);
    }

}
